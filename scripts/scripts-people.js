let people = [];

// consume api
async function getPeople() {
  const response = await fetch('https://swapi.dev/api/people/');
  people = await response.json(); // parsea response a json
  people = people.results
  console.log('PEOPLE', people)
  }

function orderPeopleByName() {
  people.sort( (a, b) => {
    if (a.name < b.name) {
        return -1;
    }
    if (b.name < a.name) {
        return 1;
    }
    return 0;
  });
  for (i = 0; i < people.length; i++) {
    document.writeln((i+1) + ': ' + people[i].name + '</br>' );
  }
}

function orderPeopleByGender() {
  people.sort( (a, b) => {
    if (a.gender < b.gender) {
        return -1;
    }
    if (b.gender < a.gender) {
        return 1;
    }
    return 0;
  });
  for (i = 0; i < people.length; i++) {
    document.writeln((i+1) + ': ' + people[i].gender + '</br>' );
  }
}

function orderPeopleByHeight() {
  people.sort( (a, b) => {
    if (a.height < b.height) {
        return -1;
    }
    if (b.height < a.height) {
        return 1;
    }
    return 0;
  });
  for (i = 0; i < people.length; i++) {
    document.writeln((i+1) + ': ' + people[i].height + '</br>' );
  }
}
