let films = [];  // arreglo

// consume api
async function getFilms() {
  const response = await fetch('https://swapi.dev/api/films/');
  films = await response.json(); // parsea response a json
  films = films.results
  console.log('PELIS', films)
    
}


function orderFilmsByTitle() {
    films.sort(function (a, b) {
    if (a.title < b.title) {
        return -1;
    }
    if (b.title < a.title) {
        return 1;
    }
    return 0;
  });
  for (i = 0; i < films.length; i++) {
    document.writeln((i+1) + ': ' + films[i].title + ' - ' + films[i].title + '</br>' );
  }
  
}

function orderFilmsByEpisode() {
  films.sort(function (a, b) {
  if (a.episode_id < b.episode_id) {
      return -1;
  }
  if (b.episode_id < a.episode_id) {
      return 1;
  }
  return 0;
});
for (i = 0; i < films.length; i++) {
  document.writeln((i+1) + ': ' + films[i].episode_id + ' - ' + films[i].episode_id + '</br>' );
}
}

function orderFilmsByDirector() {
  films.sort( (a, b) => {
    if (a.director < b.director) {
        return -1;
    }
    if (b.director < a.director) {
        return 1;
    }
    return 0;
  });
  for (i = 0; i < films.length; i++) {
    document.writeln((i+1) + ': ' + films[i].director + '</br>' );
  }
}

function orderFilmsByDate() {
  films.sort( (a, b) => {
    if (a.release_date < b.release_date ) {
        return -1;
    }
    if (b.release_date < a.release_date) {
        return 1;
    }
    return 0;
  });
  for (i = 0; i < films.length; i++) {
    document.writeln((i+1) + ': ' + films[i].release_date + '</br>' );
  }
}