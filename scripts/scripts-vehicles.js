let vehicles = [];

// consume api
async function getVehicles() {
  const response = await fetch('https://swapi.dev/api/vehicles/');
  vehicles = await response.json(); // parsea response a json
  vehicles = vehicles.results
  console.log(vehicles)
}

function orderVehiclesByName() {
    vehicles.sort(function (a, b) {
    if (a.name < b.name) {
        return -1;
    }
    if (b.name < a.name) {
        return 1;
    }
    return 0;
  });
  for (i = 0; i < vehicles.length; i++) {
    document.writeln((i+1) + ': ' + vehicles[i].name + ' - ' + vehicles[i].name + '</br>' );
  }

}

function orderVehiclesByDate() {
  vehicles.sort(function (a, b) {
  if (a.created < b.created) {
      return -1;
  }
  if (b.created < a.created) {
      return 1;
  }
  return 0;
});
for (i = 0; i < vehicles.length; i++) {
  document.writeln((i+1) + ': ' + vehicles[i].name + ' - ' + getDate(vehicles[i].created) + '</br>' );
}

}

function getDate(str) {
  let ops = {year: 'numeric'};
  ops.month = ops.day = '2-digit';
  return new Date(str).toLocaleDateString(0, ops);
}