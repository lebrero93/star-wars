let starships = [];

// consume api
async function getStarships() {
  const response = await fetch('https://swapi.dev/api/starships/');
  starships = await response.json(); // parsea response a json
  starships = starships.results
  console.log('NAVES', starships)
}

function orderStarshipsByName() {
    starships.sort(function (a, b) {
    if (a.name < b.name) {
        return -1;
    }
    if (b.name < a.name) {
        return 1;
    }
    return 0;
  });
  for (i = 0; i < starships.length; i++) {
    document.writeln((i+1) + ': ' + starships[i].name + ' - ' + starships[i].name + '</br>' );
  }
  console.log('NAVES ORDENADAS', starships)
}

function orderStarshipsByCost() {
  starships.sort(function (a, b) {
  if (a.cost_in_credits < b.cost_in_credits) {
      return -1;
  }
  if (b.cost_in_credits < a.cost_in_credits) {
      return 1;
  }
  return 0;
});
for (i = 0; i < starships.length; i++) {
  document.writeln((i+1) + ': ' + starships[i].cost_in_credits + ' - ' + starships[i].cost_in_credits + '</br>' );
}
console.log('NAVES ORDENADAS', starships)
}

function orderStarshipsByPassengers() {
  starships.sort(function (a, b) {
  if (a.passengers < b.passengers) {
      return -1;
  }
  if (b.passengers < a.passengers) {
      return 1;
  }
  return 0;
});
for (i = 0; i < starships.length; i++) {
  document.writeln((i+1) + ': ' + starships[i].name + ' - ' + starships[i].passengers + '</br>' );
}
console.log('NAVES ORDENADAS', starships)
}
