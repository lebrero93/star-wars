 let planets = [];

  // consume api
  async function getPlanets() {
    const response = await fetch('https://swapi.dev/api/planets/');
    planets = await response.json(); // parsea response a json
    planets = planets.results
    
  }

  function orderPlanetsByName() {
    planets.sort(function (a, b) {
      if (a.name < b.name) {
          return -1;
      }
      if (b.name < a.name) {
          return 1;
      }
      return 0;
    });
    for (i = 0; i < planets.length; i++) {
      document.writeln((i+1) + ': ' + planets[i].name + ' - ' + planets[i].name + '</br>' );
    }
    
  }

  function orderPlanetsByPopulation() {
    planets.sort(function (a, b) {
      if (a.population < b.population) {
          return -1;
      }
      if (b.population < a.population) {
          return 1;
      }
      return 0;
    });
    for (i = 0; i < planets.length; i++) {
      document.writeln((i+1) + ': ' + planets[i].population + ' - ' + planets[i].population + '</br>' );
    }
    
  }


  